<?php

Route::group(['prefix' => 'reports', 'as' => 'reports.'], function () {
    Route::get('source-list', [\Besoft\ReportEngine\Http\Controllers\ReportSourceController::class, 'index'])
        ->name('source.index');
    Route::post('source/store', [\Besoft\ReportEngine\Http\Controllers\ReportSourceController::class, 'store'])
        ->name('source.store');
    Route::post('source/{sourceId}/update', [\Besoft\ReportEngine\Http\Controllers\ReportSourceController::class, 'update'])
        ->name('source.update');
    Route::get('source/{sourceId}/destroy', [\Besoft\ReportEngine\Http\Controllers\ReportSourceController::class, 'destroy'])
        ->name('source.destroy');

    Route::get("/list", [\Besoft\ReportEngine\Http\Controllers\ReportController::class, 'index'])->name("list");
    Route::get('create/{reportSource}/view', [\Besoft\ReportEngine\Http\Controllers\ReportController::class, 'viewCreateReport'])->name('create.view');
    Route::get("/generate/", [\Besoft\ReportEngine\Http\Controllers\ReportController::class, "generateReport"])->name("generate");
    Route::get("/create/report-view", [\Besoft\ReportEngine\Http\Controllers\ReportController::class, "viewCreateReport"])->name("create.report.view");
    Route::get("/create/report-view/{reportId}", [\Besoft\ReportEngine\Http\Controllers\ReportController::class, "editReport"])->name("edit.view");
    Route::post("/create/report", [\Besoft\ReportEngine\Http\Controllers\ReportController::class, "createReport"])->name("store");
    Route::post("/update/{report}", [\Besoft\ReportEngine\Http\Controllers\ReportController::class, "updateReport"])->name("update");
    Route::get("/destroy/{report}", [\Besoft\ReportEngine\Http\Controllers\ReportController::class, "destroyReport"])->name("destroy");
    Route::get("/construct", [\Besoft\ReportEngine\Http\Controllers\ReportController::class, "generateReportView"])->name("construct");
});


