<?php


namespace Besoft\ReportEngine\Http\Controllers;


use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class ParentController extends Controller
{

    /**function to check if a view exist
     *
     * @param $view_name
     *
     * @return mixed
     */
    public function isViewExist($view_name)
    {
        return DB::select("SELECT * FROM information_schema.VIEWS WHERE TABLE_NAME='$view_name'");

    }

}
