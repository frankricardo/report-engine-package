<?php

namespace Besoft\ReportEngine\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ReportSource extends Model
{
    use HasFactory;
}
