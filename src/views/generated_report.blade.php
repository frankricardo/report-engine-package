@extends('reports::master')
@section("title",' Report Results')


@section("content")
    <div class="container">
    @include('reports::partial._alerts')
    <div class="card mt-5">
        <div class="card-header d-flex justify-content-between">
            <div class="card-title">
                <h3 id="title" class="card-label">{{$report->report_name}}</h3>
            </div>
            <div class="card-toolbar">
                <a href="{{route("reports.construct")}}" class="btn btn-outline-primary btn-sm"><span class="fa fa-backward"></span> Back</a>
                &nbsp;&nbsp;<div class="btn-group">
                    <a id="download-dropdown" class="btn btn-primary  dropdown-toggle btn-sm" data-toggle="dropdown"
                       aria-haspopup="true" aria-expanded="false"><span class="fa fa-file-download"></span> Download
                    </a>
                    <div class="dropdown-menu" style="">
                        <a target="_blank" id="excel" href="{{request()->fullUrl()."&download=1"}}" class="dropdown-item"
                           data-url=""> EXCEL
                        </a>
                        <a id="ajax-url" href="{{request()->fullUrl()}}"></a>
                    </div>
                </div>
            </div>
            <!--end::Dropdown-->
            <!--begin::Modal-->
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-separate table-head-custom table-checkable" id="kt_datatable1">
                    <thead>
                    <tr>
                        @foreach($columns as $column)
                            <th>{{$column}}</th>
                        @endforeach

                    </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
            <!--end: Datatable-->
        </div>
    </div>
    </div>

    <!--end::Notice-->


@stop

@section('scripts')
    <script>
        $(document).ready(function (){
            var listOfObjects = [];
            $.each({!! $columnNames !!}, function (index, value) {
                var singleObj = {};
                singleObj['data'] = value["name"];
                singleObj['name'] = value["name"];
                listOfObjects.push(singleObj);
            });

            let table =$('#kt_datatable1').DataTable({
                processing: true,
                serverSide: true,
                // dom: 'Bfrtip',
                language: {
                    processing: '<i style="color:#3C8DBC" class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span> '},
                ajax: $('#ajax-url').attr("href"),
                columns: listOfObjects,
                lengthMenu: [[10,25, 100, -1], [10,25, 100, "All"]],
                pageLength: 10,
                order: [[0, 'desc']]
            });
        })




    </script>
@endsection
