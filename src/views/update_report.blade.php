@extends('reports::master')
@section("title",'Update Report')


@section("content")
    <div class="container">
        <div class="card mt-5">
            <!--begin::Header-->
            <div class="card-header">
                <h4 class="card-title align-items-start flex-column">
                    <span class="card-label font-weight-bolder text-dark"> Update Report</span>
                </h4>
            </div>
            <!--end::Header-->
            <!--begin::Body-->
            <div class="card-body mt-3">
                <!--begin::Table-->
                <form method="post" action="{{route("reports.update",$systemReport->id)}}">
                    @csrf
                    <div class="row">
                        <div class="form-group col-6">
                            <label for="end_date"> Report Name </label>
                            <input  type="text" value="{{$systemReport->report_name}}" name="report_name" placeholder="Enter Report Name" class="form-control" >
                        </div>
                        <div class="form-group col-6">
                            <label for="end_date"> Report Source </label>
                            <input readonly=""  type="text"  value="{{$systemReport->data_source}}" name="report_source" placeholder="Enter Report Name" class="form-control" >
                        </div>
                        <div class="col-6 px-3 mb-2" style="padding: 2px">
                            <label for="end_date">  </label>
                            <div class="card  " style="padding-left:  5px">
                                <div class="row">
                                    <label class="col-8 col-form-label">
                                        <span class="switch switch-outline switch-icon switch-success">
                                                                 <i></i><label style="margin: 0">
																	<input type="checkbox" @if($systemReport->require_date_filter==1)
                                                                    checked=""
                                                                           @endif
                                                                           id="require_date_filter"
                                                                           name="require_date_filter">
																	<span></span>

																</label>
                                        </span> Require Date Filter ?</label>

                                </div>
                            </div>
                        </div>
                        <div class="col-6 px-3 mb-2" style="padding: 2px">
                            <label for="end_date">  </label>
                            <div class="card  " style="padding-left:  5px">
                                <div class="row">
                                    <label class="col-8 col-form-label"> <span class="switch switch-outline switch-icon switch-success">
                                                                 <i></i>
																<label style="margin: 0">
																	<input type="checkbox" @if($systemReport->is_active==1)
                                                                    checked=""
                                                                           @endif
                                                                           name="is_active">
																	<span></span>

																</label>
															</span> Is Report Active ?</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group col-6 {{$systemReport->require_date_filter==0?"hide":""}}" id="date_filter_container">
                            <label for="end_date"> Date Filter </label>
                            <select id="date_filter" name="date_filter" class="form-control">
                                <option value=""> --SELECT--</option>
                                @foreach($columns as $column)
                                    <option @if($systemReport->date_filter==$column) selected @endif value="{{$column}}">{{$column}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-6 {{$systemReport->require_date_filter==0?"hide":""}}" id="date_filter_desc_container">
                            <label for="end_date"> Date Filter Description </label>
                            <input  type="text" value="{{$systemReport->date_filter_description}}"
                                    name="date_filter_description"
                                    placeholder="Enter Date Filter Description"
                                    class="form-control" >
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <h3>Column List</h3>
                        </div>
                        @foreach($columns as $column)
                            <div class="col-4" style="padding: 2px">
                                <div class="card  " style="padding-left:  5px">
                                    <div class="row">
                                        <label class="col-12 col-form-label  "><span class="switch switch-outline switch-icon switch-success">
                                                                 <i></i>
																<label style="margin: 0">
																	<input type="checkbox"
                                                                           @if(in_array($column,explode(",",$systemReport->column_list))) checked @endif
                                                                           value="{{$column}}"
                                                                           name="columnList[]">
																	<span></span>

																</label>
															</span> {{$column}}</label>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                    <div class="row mt-5">
                        <div class="col-12">
                            <button type="submit" class="btn btn-primary btn-sm mb-5"><span class="la la-check-circle-o"></span> Update Report</button>
                        </div>
                    </div>
                </form>
                <!--end::Table-->
            </div>
            <!--end::Body-->
        </div>
    </div>


    <!--end::Notice-->


@stop

@section('scripts')
    <script>
        $('.nav-reporting').addClass('menu-item-active  menu-item-open');
        $('.nav-report-list').addClass('menu-item-active');
        $("#require_date_filter").click(function (e) {
            if($('#require_date_filter').is(":checked")){
                $("#date_filter_container").show();
                $("#date_filter_desc_container").show();
            }else {
                $("#date_filter_container").hide();
                $("#date_filter_desc_container").hide();

                $("#date_filter").val("");
                $("#date_filter_desc").val("");
            }



        });
    </script>
@endsection
