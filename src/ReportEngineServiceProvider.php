<?php

namespace Besoft\ReportEngine;


use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider;

class ReportEngineServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        // register routes
        include __DIR__.'/routes/report.php';




    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
        $this->loadRoutesFrom(__DIR__ . '/routes/report.php');
        $this->loadViewsFrom(__DIR__.'/views','reports');
        $this->loadMigrationsFrom(__DIR__.'/database/migrations');
        $this->publishes([
             include __DIR__.'/routes/report.php',
//            __DIR__.'/routes'=>__DIR__.'/routes/',
            __DIR__.'/views' => resource_path('views/vendor/reports'),
            __DIR__.'/database/migrations/' => database_path('migrations'),
            __DIR__.'/http/Controllers/ParentController.php' => app_path('Http/Controllers/ParentController.php'),
            __DIR__.'/http/Controllers/ReportController.php' => app_path('Http/Controllers/ReportController.php'),
            __DIR__.'/http/Controllers/ReportSourceController.php' => app_path('Http/Controllers/ReportSourceController.php'),
        ]);



    }
}
