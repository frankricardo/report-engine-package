<?php return array (
  'root' => 
  array (
    'pretty_version' => '1.0.0+no-version-set',
    'version' => '1.0.0.0',
    'aliases' => 
    array (
    ),
    'reference' => NULL,
    'name' => 'besoft/report-engine',
  ),
  'versions' => 
  array (
    'besoft/report-engine' => 
    array (
      'pretty_version' => '1.0.0+no-version-set',
      'version' => '1.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'doctrine/inflector' => 
    array (
      'pretty_version' => '2.0.3',
      'version' => '2.0.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '9cf661f4eb38f7c881cac67c75ea9b00bf97b210',
    ),
    'doctrine/lexer' => 
    array (
      'pretty_version' => '1.2.1',
      'version' => '1.2.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e864bbf5904cb8f5bb334f99209b48018522f042',
    ),
    'egulias/email-validator' => 
    array (
      'pretty_version' => '2.1.25',
      'version' => '2.1.25.0',
      'aliases' => 
      array (
      ),
      'reference' => '0dbf5d78455d4d6a41d186da50adc1122ec066f4',
    ),
    'ezyang/htmlpurifier' => 
    array (
      'pretty_version' => 'v4.13.0',
      'version' => '4.13.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '08e27c97e4c6ed02f37c5b2b20488046c8d90d75',
    ),
    'hashids/hashids' => 
    array (
      'pretty_version' => '4.1.0',
      'version' => '4.1.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '8cab111f78e0bd9c76953b082919fc9e251761be',
    ),
    'illuminate/bus' => 
    array (
      'pretty_version' => 'v8.64.0',
      'version' => '8.64.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'a222094903c473b6b0ade0b0b0e20b83ae1472b6',
    ),
    'illuminate/collections' => 
    array (
      'pretty_version' => 'v8.64.0',
      'version' => '8.64.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'edaf5ea6ffe63dcab1ac86ee9db9c65209f44813',
    ),
    'illuminate/console' => 
    array (
      'pretty_version' => 'v8.64.0',
      'version' => '8.64.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c1bfcf8cde25f35b636ae5bd7df4502e8179e273',
    ),
    'illuminate/container' => 
    array (
      'pretty_version' => 'v8.64.0',
      'version' => '8.64.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'ef73feb5216ef97ab7023cf59c0c8dbbd5505a9d',
    ),
    'illuminate/contracts' => 
    array (
      'pretty_version' => 'v8.64.0',
      'version' => '8.64.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'ab4bb4ec3b36905ccf972c84f9aaa2bdd1153913',
    ),
    'illuminate/database' => 
    array (
      'pretty_version' => 'v8.64.0',
      'version' => '8.64.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'b8c1e5393e8c7f087a7f79158d04f314f00422d0',
    ),
    'illuminate/events' => 
    array (
      'pretty_version' => 'v8.64.0',
      'version' => '8.64.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'b7f06cafb6c09581617f2ca05d69e9b159e5a35d',
    ),
    'illuminate/filesystem' => 
    array (
      'pretty_version' => 'v8.64.0',
      'version' => '8.64.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f33219e5550f8f280169e933b91a95250920de06',
    ),
    'illuminate/http' => 
    array (
      'pretty_version' => 'v8.64.0',
      'version' => '8.64.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '6e4cc1ff7c7eb923fb5dff2bc2d8db831c8b8e7b',
    ),
    'illuminate/macroable' => 
    array (
      'pretty_version' => 'v8.64.0',
      'version' => '8.64.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '300aa13c086f25116b5f3cde3ca54ff5c822fb05',
    ),
    'illuminate/pipeline' => 
    array (
      'pretty_version' => 'v8.64.0',
      'version' => '8.64.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '23aeff5b26ae4aee3f370835c76bd0f4e93f71d2',
    ),
    'illuminate/routing' => 
    array (
      'pretty_version' => 'v8.64.0',
      'version' => '8.64.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '124738cfbbb46d197ed15a2820fe77ffa174eae7',
    ),
    'illuminate/session' => 
    array (
      'pretty_version' => 'v8.64.0',
      'version' => '8.64.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '9ed81f37664392e09d55058c96811d791311df21',
    ),
    'illuminate/support' => 
    array (
      'pretty_version' => 'v8.64.0',
      'version' => '8.64.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '5346222c24bbc0da88af032053328dbebd1ab2d8',
    ),
    'illuminate/translation' => 
    array (
      'pretty_version' => 'v8.64.0',
      'version' => '8.64.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '28219a6b98b2f1de529c3a821c4efe494f4247d3',
    ),
    'illuminate/validation' => 
    array (
      'pretty_version' => 'v8.64.0',
      'version' => '8.64.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '50316b9fc8b64ac2ca8e752c913b9baa2b26f785',
    ),
    'illuminate/view' => 
    array (
      'pretty_version' => 'v8.64.0',
      'version' => '8.64.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'cc2804231d6249d0bc6ac5fc856f1ce926a07dcd',
    ),
    'laravelcollective/html' => 
    array (
      'pretty_version' => 'v6.2.1',
      'version' => '6.2.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'ae15b9c4bf918ec3a78f092b8555551dd693fde3',
    ),
    'league/fractal' => 
    array (
      'pretty_version' => '0.19.2',
      'version' => '0.19.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '06dc15f6ba38f2dde2f919d3095d13b571190a7c',
    ),
    'maatwebsite/excel' => 
    array (
      'pretty_version' => '3.1.33',
      'version' => '3.1.33.0',
      'aliases' => 
      array (
      ),
      'reference' => 'b2de5ba92c5c1ad9415f0eb7c72838fb3eaaa5b8',
    ),
    'maennchen/zipstream-php' => 
    array (
      'pretty_version' => '2.1.0',
      'version' => '2.1.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c4c5803cc1f93df3d2448478ef79394a5981cc58',
    ),
    'markbaker/complex' => 
    array (
      'pretty_version' => '2.0.3',
      'version' => '2.0.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '6f724d7e04606fd8adaa4e3bb381c3e9db09c946',
    ),
    'markbaker/matrix' => 
    array (
      'pretty_version' => '2.1.3',
      'version' => '2.1.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '174395a901b5ba0925f1d790fa91bab531074b61',
    ),
    'myclabs/php-enum' => 
    array (
      'pretty_version' => '1.8.3',
      'version' => '1.8.3.0',
      'aliases' => 
      array (
      ),
      'reference' => 'b942d263c641ddb5190929ff840c68f78713e937',
    ),
    'nesbot/carbon' => 
    array (
      'pretty_version' => '2.53.1',
      'version' => '2.53.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f4655858a784988f880c1b8c7feabbf02dfdf045',
    ),
    'phpoffice/phpspreadsheet' => 
    array (
      'pretty_version' => '1.18.0',
      'version' => '1.18.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '418cd304e8e6b417ea79c3b29126a25dc4b1170c',
    ),
    'psr/container' => 
    array (
      'pretty_version' => '1.1.1',
      'version' => '1.1.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '8622567409010282b7aeebe4bb841fe98b58dcaf',
    ),
    'psr/container-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0',
      ),
    ),
    'psr/event-dispatcher' => 
    array (
      'pretty_version' => '1.0.0',
      'version' => '1.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'dbefd12671e8a14ec7f180cab83036ed26714bb0',
    ),
    'psr/event-dispatcher-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0',
      ),
    ),
    'psr/http-client' => 
    array (
      'pretty_version' => '1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '2dfb5f6c5eff0e91e20e913f8c5452ed95b86621',
    ),
    'psr/http-factory' => 
    array (
      'pretty_version' => '1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '12ac7fcd07e5b077433f5f2bee95b3a771bf61be',
    ),
    'psr/http-message' => 
    array (
      'pretty_version' => '1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f6561bf28d520154e4b0ec72be95418abe6d9363',
    ),
    'psr/log' => 
    array (
      'pretty_version' => '1.1.4',
      'version' => '1.1.4.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd49695b909c3b7628b6289db5479a1c204601f11',
    ),
    'psr/log-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0|2.0',
      ),
    ),
    'psr/simple-cache' => 
    array (
      'pretty_version' => '1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '408d5eafb83c57f6365a3ca330ff23aa4a5fa39b',
    ),
    'symfony/console' => 
    array (
      'pretty_version' => 'v5.3.7',
      'version' => '5.3.7.0',
      'aliases' => 
      array (
      ),
      'reference' => '8b1008344647462ae6ec57559da166c2bfa5e16a',
    ),
    'symfony/deprecation-contracts' => 
    array (
      'pretty_version' => 'v2.4.0',
      'version' => '2.4.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '5f38c8804a9e97d23e0c8d63341088cd8a22d627',
    ),
    'symfony/error-handler' => 
    array (
      'pretty_version' => 'v5.3.7',
      'version' => '5.3.7.0',
      'aliases' => 
      array (
      ),
      'reference' => '3bc60d0fba00ae8d1eaa9eb5ab11a2bbdd1fc321',
    ),
    'symfony/event-dispatcher' => 
    array (
      'pretty_version' => 'v5.3.7',
      'version' => '5.3.7.0',
      'aliases' => 
      array (
      ),
      'reference' => 'ce7b20d69c66a20939d8952b617506a44d102130',
    ),
    'symfony/event-dispatcher-contracts' => 
    array (
      'pretty_version' => 'v2.4.0',
      'version' => '2.4.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '69fee1ad2332a7cbab3aca13591953da9cdb7a11',
    ),
    'symfony/event-dispatcher-implementation' => 
    array (
      'provided' => 
      array (
        0 => '2.0',
      ),
    ),
    'symfony/finder' => 
    array (
      'pretty_version' => 'v5.3.7',
      'version' => '5.3.7.0',
      'aliases' => 
      array (
      ),
      'reference' => 'a10000ada1e600d109a6c7632e9ac42e8bf2fb93',
    ),
    'symfony/http-client-contracts' => 
    array (
      'pretty_version' => 'v2.4.0',
      'version' => '2.4.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '7e82f6084d7cae521a75ef2cb5c9457bbda785f4',
    ),
    'symfony/http-foundation' => 
    array (
      'pretty_version' => 'v5.3.7',
      'version' => '5.3.7.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e36c8e5502b4f3f0190c675f1c1f1248a64f04e5',
    ),
    'symfony/http-kernel' => 
    array (
      'pretty_version' => 'v5.3.9',
      'version' => '5.3.9.0',
      'aliases' => 
      array (
      ),
      'reference' => 'ceaf46a992f60e90645e7279825a830f733a17c5',
    ),
    'symfony/mime' => 
    array (
      'pretty_version' => 'v5.3.8',
      'version' => '5.3.8.0',
      'aliases' => 
      array (
      ),
      'reference' => 'a756033d0a7e53db389618653ae991eba5a19a11',
    ),
    'symfony/polyfill-ctype' => 
    array (
      'pretty_version' => 'v1.23.0',
      'version' => '1.23.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '46cd95797e9df938fdd2b03693b5fca5e64b01ce',
    ),
    'symfony/polyfill-intl-grapheme' => 
    array (
      'pretty_version' => 'v1.23.1',
      'version' => '1.23.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '16880ba9c5ebe3642d1995ab866db29270b36535',
    ),
    'symfony/polyfill-intl-idn' => 
    array (
      'pretty_version' => 'v1.23.0',
      'version' => '1.23.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '65bd267525e82759e7d8c4e8ceea44f398838e65',
    ),
    'symfony/polyfill-intl-normalizer' => 
    array (
      'pretty_version' => 'v1.23.0',
      'version' => '1.23.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '8590a5f561694770bdcd3f9b5c69dde6945028e8',
    ),
    'symfony/polyfill-mbstring' => 
    array (
      'pretty_version' => 'v1.23.1',
      'version' => '1.23.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '9174a3d80210dca8daa7f31fec659150bbeabfc6',
    ),
    'symfony/polyfill-php72' => 
    array (
      'pretty_version' => 'v1.23.0',
      'version' => '1.23.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '9a142215a36a3888e30d0a9eeea9766764e96976',
    ),
    'symfony/polyfill-php73' => 
    array (
      'pretty_version' => 'v1.23.0',
      'version' => '1.23.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'fba8933c384d6476ab14fb7b8526e5287ca7e010',
    ),
    'symfony/polyfill-php80' => 
    array (
      'pretty_version' => 'v1.23.1',
      'version' => '1.23.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '1100343ed1a92e3a38f9ae122fc0eb21602547be',
    ),
    'symfony/process' => 
    array (
      'pretty_version' => 'v5.3.7',
      'version' => '5.3.7.0',
      'aliases' => 
      array (
      ),
      'reference' => '38f26c7d6ed535217ea393e05634cb0b244a1967',
    ),
    'symfony/routing' => 
    array (
      'pretty_version' => 'v5.3.7',
      'version' => '5.3.7.0',
      'aliases' => 
      array (
      ),
      'reference' => 'be865017746fe869007d94220ad3f5297951811b',
    ),
    'symfony/service-contracts' => 
    array (
      'pretty_version' => 'v2.4.0',
      'version' => '2.4.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f040a30e04b57fbcc9c6cbcf4dbaa96bd318b9bb',
    ),
    'symfony/string' => 
    array (
      'pretty_version' => 'v5.3.7',
      'version' => '5.3.7.0',
      'aliases' => 
      array (
      ),
      'reference' => '8d224396e28d30f81969f083a58763b8b9ceb0a5',
    ),
    'symfony/translation' => 
    array (
      'pretty_version' => 'v5.3.9',
      'version' => '5.3.9.0',
      'aliases' => 
      array (
      ),
      'reference' => '6e69f3551c1a3356cf6ea8d019bf039a0f8b6886',
    ),
    'symfony/translation-contracts' => 
    array (
      'pretty_version' => 'v2.4.0',
      'version' => '2.4.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '95c812666f3e91db75385749fe219c5e494c7f95',
    ),
    'symfony/translation-implementation' => 
    array (
      'provided' => 
      array (
        0 => '2.3',
      ),
    ),
    'symfony/var-dumper' => 
    array (
      'pretty_version' => 'v5.3.8',
      'version' => '5.3.8.0',
      'aliases' => 
      array (
      ),
      'reference' => 'eaaea4098be1c90c8285543e1356a09c8aa5c8da',
    ),
    'voku/portable-ascii' => 
    array (
      'pretty_version' => '1.5.6',
      'version' => '1.5.6.0',
      'aliases' => 
      array (
      ),
      'reference' => '80953678b19901e5165c56752d087fc11526017c',
    ),
    'yajra/laravel-datatables' => 
    array (
      'pretty_version' => 'v1.5.0',
      'version' => '1.5.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '50de5e20ef01da1a353e0a81c0ad5f9da6a985ec',
    ),
    'yajra/laravel-datatables-buttons' => 
    array (
      'pretty_version' => 'v4.13.3',
      'version' => '4.13.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '5c8fb97c26c14408c8933f68d86c6e4823d05740',
    ),
    'yajra/laravel-datatables-editor' => 
    array (
      'pretty_version' => 'v1.24.2',
      'version' => '1.24.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '8418f7f2fb4fb73e095687c464a50d92e9f1f357',
    ),
    'yajra/laravel-datatables-fractal' => 
    array (
      'pretty_version' => 'v1.6.0',
      'version' => '1.6.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '0aa387a9b3738248fa61110f0378904ef42b4a73',
    ),
    'yajra/laravel-datatables-html' => 
    array (
      'pretty_version' => 'v4.41.0',
      'version' => '4.41.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '9e5d9edf397a7311751d09377a93b47aafe7e77b',
    ),
    'yajra/laravel-datatables-oracle' => 
    array (
      'pretty_version' => 'v9.18.1',
      'version' => '9.18.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '7148225d52bcdfdd77c24e8d456058f1150b84e7',
    ),
  ),
);
